*** Settings ***
Documentation       tkt

Resource    TestSteps.resource

*** Test Cases ***
Make an appointment

    Given I'm in Home Page    https://katalon-demo-cura.herokuapp.com/

    When Go to Login Page and connect    https://katalon-demo-cura.herokuapp.com/profile.php#login

    Then I'm in Appointment Page    https://katalon-demo-cura.herokuapp.com/#appointment

    When Fill fo rm to make an appointment

    Then J'ai rien écris